#LOCATES RGB EPS IMAGES
*Written for Hirt and Carter 2014
*Written by Ventum a/s info@ventum.co.za
*V2

NOTE: This version no longer relies on building a cache file. But you need to point the script to the full path you want the script to search. 

You might need to make the file executable

```
#!bash

chmod a+x ./RGB_snooper.pl
```

Execute

```
#!bash

./RGB_snooper.pl /path/to/search
```