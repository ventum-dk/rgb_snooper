#!/usr/bin/perl 
#LOCATES RGB EPS IMAGES
#WRITTEN FOR HIRT AND CARTER 2014
#WRITTEN BY VENTUM A/S info@ventum.co.za
#V2

#You might need to make the file executable
#chmod a+x ./RGB_snooper.pl

#Execute
#./RGB_snooper.pl /path/to/search

my $searchpath = $ARGV[0];
if (!$searchpath){
	print "You need to supply searchpath.\ne.g. RGB_snooper.pl /path/to/search\n";
	exit;
}

my $find = "/usr/bin/find";
my $cache = '/var/tmp/search-rgb.cache';

print "Creating file cache\n";
my $search = "$find '$searchpath' -type f \\( ! -iname '.*' \\) > $cache";
#print "$search\n";
`$search`;

my $pathss = `cat $cache`;

#split SAP into rows \n
my @myarr = split('\n', $pathss);
my $arrcount = scalar(@myarr) - 2; #counts array items
my $i = -1;

while ($i++ <= $arrcount) {
	`clear`;
	print "No. $i - $myarr[$i]";
	&is_rgb ("$myarr[$i]");
	print "\n";	
}

print "\nSee the output report files:\nRGB_finds.txt\nCMYK_finds.txt\nOther.txt\nDone:\nThank you Ventum\n";
exit;


sub print_to_file{
	#Description: prints log to file
	#Usage: &print_to_file($log, "filename");
	$log = $_[0];
	$log_name = $_[1];
	
	open(my $fh, '>>', "$log_name");
	print $fh "$log\n";
	close $fh;
	#print "$log\n";
}

sub is_rgb{
	$path = $_[0];
	
	$info = `/usr/etc/appletalk/imagetox -xinf "$path" |grep "colorspace"`;
	chomp($info);
	
	$log = "$path:\t$info"; 
	
	if ($info eq "colorspace: RGB"){
		&print_to_file($log, "RGB_finds.txt");
	}elsif ($info eq "colorspace: CMYK"){	
		&print_to_file($log, "CMYK_finds.txt");
	}else{
		&print_to_file($log, "Other.txt");
	}
	
	return $log;
}
